﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlainGeometricFiguresLibrary;
namespace PlainGeometricFigures
{

    class Program
    {
        static void Main(string[] args)
        {
            string tostring = null;
            Square sq = new Square(4);
            string figuraGeo = sq.Introduce();
            sq.LoadFromFile(@"C:\Users\lattitude\Desktop\figuraGeo.txt");
            sq.SaveFromFile(@"C:\Users\lattitude\Desktop\figuraGeo.txt");

            var result = new StringBuilder();
            var figures = new List<PlainGeometricFiguresLibrary.PlainGeometricFigures>()
            {
                new Square(1),
                new Square(2),
                new Rectangle(1,2),
                new EquilateralTriangle(1),
                new EquilateralTriangle(2),
                new RightAngleTriangle(3,4)
            };
            double sum = 0;
            double sumImp = 0;
            var res = new List<PlainGeometricFiguresLibrary.PlainGeometricFigures>();
            var res1 = new List<PlainGeometricFiguresLibrary.PlainGeometricFigures>();
            foreach (var fig in figures)
            {


                result.AppendLine(fig.Introduce());
                result.AppendLine();

                fig.PaintCompleted += P_PaintCompleted;

                fig.Paint(Colors.Blue);

                fig.CalculateTheArea += C_CalculateTheArea;

                fig.CalculateArea();
                sumImp += fig;

            }
            Rectangle rect = new Rectangle(3, 4);
            rect.CalculateArea();
            sum = Sum(sq, rect);

            Console.WriteLine("sum is ={0}", sum);
            Console.WriteLine("Total sum is ={0}", sumImp);
            res = figures.OrderByArea();
            res1 = figures.OrderByArea(true);
            foreach (var i in res)
            { tostring = string.Format("gli elementi in modo ordinati crescenti e: {0}", i.ToString()); Console.WriteLine(tostring); }
            foreach (var j in res1)
            {
                tostring = string.Format("gli elementi in modo ordinati decrescenti e: {0}", j.ToString()); Console.WriteLine(tostring);
            }


            Console.WriteLine(result.ToString());
            Console.ReadLine();
        }

        private static void C_CalculateTheArea(object sender, CalculateTheArea e)
        {
        }

        private static void P_PaintCompleted(object sender, PaintCompleted e)
        {

        }
        static double Sum(double area1, double area2)
        {
            return area1 + area2;
        }

    }

}
