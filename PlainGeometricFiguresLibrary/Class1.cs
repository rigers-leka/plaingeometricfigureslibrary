﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PlainGeometricFiguresLibrary
{
    public enum Colors
    {
        Yellow,
        Green,
        Blue,
        Black,
    }

    public interface IPlainGeometricFigures
    {
        double Area { get; }
        string Introduce();
        event EventHandler<CalculateTheArea> CalculateTheArea;

    }

    public interface IDesign
    {
        void Paint(Colors col);
        event EventHandler<PaintCompleted> PaintCompleted;

    }

    public abstract class PlainGeometricFigures : IPlainGeometricFigures, IDesign
    {
        public abstract double Area { get; }

        public string Introduce()
        {
            return string.Format("I am a {0} and my area is {1}", GetType().Name, Area);
        }

        public event EventHandler<CalculateTheArea> CalculateTheArea;
        public event EventHandler<PaintCompleted> PaintCompleted;
        public Colors _Col { get; protected set; }
        public void Paint(Colors col)
        {
            _Col = col;
            var tempPaint = PaintCompleted;
            if (tempPaint != null)
            {
                tempPaint(this, new PaintCompleted() { Col = _Col });
            }

        }
        public double _area { get; protected set; }

        public void CalculateArea()
        {
            _area = _setArea;
        }

        public double _setArea
        {
            get { return Area; }
            set
            {
                _area = value;


                if (_area != Area)
                {
                    var tempCalculate = CalculateTheArea;
                    if (tempCalculate != null)
                        tempCalculate(this, new CalculateTheArea());

                    _area = Area;
                }

                else
                {
                    if (_area == 0)
                        Console.WriteLine(_area);

                }

            }

        }
        public override string ToString()
        {
            return String.Format("Area ={0}", Area);
        }
        public static implicit operator double(PlainGeometricFigures fig)
        {
            return fig.Area;

        }


    }

    public abstract class Triangle : PlainGeometricFigures, IPlainGeometricFigures, IDesign
    {
        protected Triangle(double baseT, double hightT)
        {
            _setArea = (baseT * hightT) / 2;
        }
        public override double Area { get { return _area; } }






    }
    public class Rectangle : PlainGeometricFigures, IPlainGeometricFigures, IDesign
    {
        public Rectangle(double baseR = 0, double highR = 0)
        {
            if ((baseR < 0) && (highR < 0))
                throw new ArgumentException();

            _setArea = baseR * highR;

        }
        public override double Area { get { return _area; } }




    }
    public class RightAngleTriangle : Triangle, IPlainGeometricFigures, IDesign
    {

        public RightAngleTriangle(double leg1 = 0, double leg2 = 0) : base(leg1, leg2)
        {

        }
    }

    public class EquilateralTriangle : Triangle, IPlainGeometricFigures, IDesign
    {
        public EquilateralTriangle(double angle = 0) : base(angle, ((Math.Sqrt(3) / 2) * angle))
        {

        }


    }

    public class Square : Rectangle, IPlainGeometricFigures, IDesign
    {
        double _Angle { get; }
        public Square(double angle = default(double)) : base(angle, angle)
        {
            _Angle = angle;
        }

        public void LoadFromFile(string path)
        {

            using (var reader = new StreamReader(path))
            {
                if (reader == null)
                {
                    throw new InvalidOperationException();
                }
                reader.ReadLine();
            }
        }
        public void SaveFromFile(string path)
        {
            using (var write = new StreamWriter(path))
            {
                write.WriteLine("My angle is {0}, and my area {1}", _Angle, Area);
            }

        }
    }

    public static class PlainGeometricFiguresExtension
    {

        public static List<PlainGeometricFigures> OrderByArea(this List<PlainGeometricFigures> PlainGeo, bool Descending = false)
        {
            var result = new List<PlainGeometricFigures>();
            if (PlainGeo.Count() == 0)
                throw new InvalidOperationException();
            foreach (var figure in PlainGeo)
            {
                result.Add(figure);
            }
            if (Descending == true)
            {
                return result.OrderByDescending((i) => i.Area).ToList();
            }
            else
                return result.OrderBy((i) => i.Area).ToList();

        }


    }

    public class CalculateTheArea : EventArgs
    {

    }
    public class PaintCompleted : EventArgs
    {
        public Colors Col { get; set; }
    }

}